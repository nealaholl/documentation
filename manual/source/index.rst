Welcome to the OpenLP User Manual
=================================

The OpenLP User Manual is here to help you learn how to use OpenLP. From installing, to setting up your monitors, to
using OpenLP on a regular basis, all the information you need to know is in here. And if you see some of it is missing,
please `help us add it`_.

Getting Started With OpenLP
---------------------------

.. toctree::
   :maxdepth: 2

   introduction
   system_requirements
   install_windows
   install_mac
   install_linux
   install_bsd

Reference Manual
----------------

.. toctree::
   :maxdepth: 2

   wizard
   menu_items
   configure
   configure_ui
   backing_up
   dualmonitors
   mediamanager
   creating_service
   print_service
   projector
   plugin_list
   display_tags
   configure_shortcuts
   themes
   songs
   bibles
   custom_slides
   export_songs
   song_usage
   alert
   web_remote
   stage_view
   android-remote
   ios-remote
   glossary

Questions and Troubleshooting
-----------------------------

.. toctree::
   :maxdepth: 2

   faq
   troubleshooting

.. _help us add it: https://openlp.org/contribute
